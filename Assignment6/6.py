import json
import requests

json_obj = requests.get('https://api.bitbucket.org/2.0/repositories/ppk-teach/tools/issues?')

data = json.loads(json_obj.text)

for item in data["values"]:
    if item["state"] == "new":
	print "Reporter: ",
	if item["reporter"] != None:
	    print item["reporter"]["display_name"]
	else:
	    print "No Name"
	print "Issue: ",
	print item["title"]
